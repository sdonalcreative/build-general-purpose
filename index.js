/*jslint node:true */
'use strict';

module.exports = function (gulp, projectRoot, createPrefixlessTasks, options) {
    var path = require('path'),
        tasksPath = path.join(__dirname, 'gulp-tasks'),
        taskPrefix = 'sdonalcreative.';

    options = options || {};

    // Load all gulp tasks, using the name of each file in the tasksPath as the name of the task.
    require('fs').readdirSync(tasksPath).forEach(
        function (filename) {
            var taskName = path.basename(filename, '.js'),
                taskOptions = options[taskName];

            gulp.task(
                taskPrefix + taskName,
                require(path.join(tasksPath, filename))(gulp, taskPrefix, projectRoot, taskOptions)
            );
        }
    );

    gulp.task(taskPrefix + 'build', [
        taskPrefix + 'html',
        taskPrefix + 'css',
        taskPrefix + 'js',
        taskPrefix + 'static'
    ]);

    gulp.task(taskPrefix + 'default', [ taskPrefix + 'build' ]);

    gulp.task(taskPrefix + 'develop', [
        taskPrefix + 'build',
        taskPrefix + 'app-server',
        taskPrefix + 'app-server-lint',
        taskPrefix + 'app-client',
        taskPrefix + 'html-watch',
        taskPrefix + 'css-watch',
        taskPrefix + 'js-watch',
        taskPrefix + 'static-watch'
    ]);

    if (createPrefixlessTasks) {
        gulp.task('build', [ taskPrefix + 'build' ]);
        gulp.task('default', [ taskPrefix + 'default' ]);
        gulp.task('develop', [ taskPrefix + 'develop' ]);
        gulp.task('clean', [ taskPrefix + 'clean' ]);
    }
};
