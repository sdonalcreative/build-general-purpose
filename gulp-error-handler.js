/*jslint node:true */
'use strict';

module.exports = function (error) {
    var chalk = require('chalk'),
        hasAnsi = require('has-ansi');

    if (hasAnsi(error.message)) {
        console.error(error.message);
    } else {
        console.error('\n' +
            chalk.green(error.plugin) + ':\n    ' +
            chalk.white.bgRed(error.name) + '\n    ' +
            error.message.trim().replace(/\n/g, '\n    ') +
        '\n');
    }

    this.emit('end');
};
