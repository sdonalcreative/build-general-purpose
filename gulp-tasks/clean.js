'use strict';

module.exports = function () {
    return function () {
        var del = require('del');

        return del([ '../dist/**/*' ], { force: true });
    };
};
