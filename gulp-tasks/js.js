'use strict';

module.exports = function (gulp) {
    return function () {
        var plumber = require('gulp-plumber'),
            errorHandler = require('../gulp-error-handler'),
            sourcemaps = require('gulp-sourcemaps'),
            uglify = require('gulp-uglify'),
            browser = require('gulp-browser'),
            merge = require('merge-stream'),
            dustify = require('dustify'),
            eslint,
            stream = merge();

        if (!process.env.NODE_ENV) {
            eslint = require('gulp-eslint');
        }

        if (!process.env.NODE_ENV) {
            stream.add(gulp.src(['client/js/**/*.js', '!client/js/lib/**'])
                .pipe(eslint())
                .pipe(eslint.format())
                .pipe(eslint.failAfterError()));
        }

        stream.add(gulp.src(['client/js/*.js', '!client/js/serviceWorker.js'])
            .pipe(plumber({ errorHandler: errorHandler }))
            .pipe(browser.browserify([
                {
                    transform: dustify,
                    options: {
                        path: 'client/js'
                    }
                }
            ]))
            .pipe(sourcemaps.init())
            .pipe(uglify())
            .pipe(sourcemaps.write('./'))
            .pipe(plumber.stop())
            .pipe(gulp.dest('../dist/js/')));

        stream.add(gulp.src('client/js/serviceWorker.js')
            .pipe(plumber({ errorHandler: errorHandler }))
            .pipe(browser.browserify())
            .pipe(sourcemaps.init())
            .pipe(uglify())
            .pipe(sourcemaps.write('./'))
            .pipe(plumber.stop())
            .pipe(gulp.dest('../dist/')));

        return stream;
    };
};
