'use strict';

module.exports = function (gulp, taskPrefix) {
    return function () {
        var watch = require('gulp-watch');

        return watch([
            'client/js/**/*.js',
            '!client/js/lib/**',
            'client/js/**/*.dust'
        ], function () {
            gulp.start(taskPrefix + 'js');
        });
    };
};
