'use strict';

module.exports = function (gulp, taskPrefix, projectRoot) {
    return function () {
        var nodemon = require('gulp-nodemon'),
            path = require('path');

        var serverModule = path.join(projectRoot, './server/');

        try {
            require.resolve(serverModule);
        } catch (ex) {
            console.warn('No server found.');
            return;
        }

        return nodemon({
            script: serverModule,
            watch: serverModule,
            tasks: [ taskPrefix + 'app-server-lint' ],
            nodeArgs: [ '--inspect' ]
        });
    };
};
