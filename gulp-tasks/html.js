'use strict';

function getConfig() {
    var path = require('path'),
        configModule = path.join(process.cwd(), './config');

    try {
        require.resolve(configModule);
    } catch (ex) {
        console.log('Can\'t find application config!');

        return {};
    }

    if (require.cache[require.resolve(configModule)]) {
        delete require.cache[require.resolve(configModule)];
    }

    return require(configModule).client || {};
}

module.exports = function (gulp, taskPrefix, projectRoot, options) {
    return function () {
        var plumber = require('gulp-plumber'),
            errorHandler = require('../gulp-error-handler'),
            dust = require('dustjs-linkedin'),
            dustHtml,
            stream;

        options = options || {};

        require('dustjs-helpers');
        require('dustjs-helper-formatdate');
        require('dust-naming-convention-filters')(dust);

        if (options.helpers) {
            for (let helper in options.helpers) {
                if (!options.helpers.hasOwnProperty(helper)) { continue; }

                dust.helpers[helper] = options.helpers[helper];
            }
        }

        dustHtml = require('gulp-dust-html');

        stream = gulp.src([
            'client/**/*.dust',
            '!client/partials/**',
            '!client/js/**'
        ]);

        if (options.preDustStream) {
            stream = stream.pipe(options.preDustStream());
        }

        return stream
            .pipe(plumber({ errorHandler: errorHandler }))
            .pipe(dustHtml({
                basePath: 'client/partials',
                data: function (file) {
                    try {
                        var path = file.path.slice(0, -5),
                            data;

                        if (options.data) {
                            data = options.data(file);
                        }

                        if (!data) {
                            if (require.cache[require.resolve(path)]) {
                                delete require.cache[require.resolve(path)];
                            }

                            data = require(path);
                        }

                        data.environment = getConfig();

                        return data;
                    } catch (ex) {
                        return { environment: getConfig() };
                    }
                },
                config: {
                    cache: false
                }
            }))
            .pipe(plumber.stop())
            .pipe(gulp.dest('../dist/'));
    };
};
