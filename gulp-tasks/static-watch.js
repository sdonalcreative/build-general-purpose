/*jslint node:true */
'use strict';

module.exports = function (gulp, taskPrefix) {
    return function () {
        var watch = require('gulp-watch');

        return watch(['client/images/**/*', 'client/assets/**/*', 'client/js/lib/**/*'], function () {
            gulp.start(taskPrefix + 'static');
        });
    };
};
