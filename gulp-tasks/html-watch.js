'use strict';

module.exports = function (gulp, taskPrefix, projectRoot, options) {
    var watchList = [
        'client/**/*.dust',
        'client/**/*.json',
        'client/**/*.js',
        'config/**/*.js',
        '!client/js/**'
    ];

    if (options && options.watch) {
        watchList = watchList.concat(options.watch);
    }

    return function () {
        var watch = require('gulp-watch');

        return watch(watchList, function () {
            gulp.start(taskPrefix + 'html');
        });
    };
};
