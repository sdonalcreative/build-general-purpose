'use strict';

module.exports = function (gulp, taskPrefix) {
    return function () {
        var watch = require('gulp-watch');

        return watch([
            'client/css/**/*.scss',
            'client/css/**/*.css'
        ], function () {
            gulp.start(taskPrefix + 'css');
        });
    };
};
