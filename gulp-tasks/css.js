'use strict';

module.exports = function (gulp) {
    return function () {
        var plumber = require('gulp-plumber'),
            errorHandler = require('../gulp-error-handler'),
            sass = require('gulp-sass'),
            postcss = require('gulp-postcss'),
            autoprefixer = require('autoprefixer'),
            nano = require('gulp-cssnano'),
            sourcemaps = require('gulp-sourcemaps');

        return gulp.src('client/css/*.scss')
            .pipe(plumber({ errorHandler: errorHandler }))
            .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(postcss(
                [
                    require('postcss-object-fit-images'),
                    autoprefixer({ browsers: ['last 2 versions', 'ie 9'] })
                ]
            ))
            .pipe(nano({
                minifyFontValues: false
            }))
            .pipe(sourcemaps.write('./'))
            .pipe(plumber.stop())
            .pipe(gulp.dest('../dist/css'));
    };
};
