'use strict';

module.exports = function (gulp) {
    return function () {
        var merge = require('merge-stream');

        return merge(
            gulp.src('client/images/**/*').pipe(gulp.dest('../dist/images/')),
            gulp.src('client/assets/**/*').pipe(gulp.dest('../dist/assets/')),
            gulp.src('client/js/lib/**/*').pipe(gulp.dest('../dist/js/lib/'))
        );
    };
};
