'use strict';

module.exports = function (gulp, taskPrefix, projectRoot, options) {
    options = options || {};



    return function () {
        var server = require('gulp-server-livereload'),
            chalk = require('chalk');

        console.log(chalk.green(
            '\nFrontend server running on ' +
            chalk.cyan('http://localhost:8001/') +
            '!\n'
        ));

        return gulp.src('./../dist/').pipe(server({
            host: '0.0.0.0',
            port: '8001',
            livereload: {
                enable: true,
                clientConsole: true
            },
            directoryListing: {
                path: './../dist/'
            },
            log: 'warn',
            clientLog: 'info',
            proxies: options.proxies
        }));
    };
};
