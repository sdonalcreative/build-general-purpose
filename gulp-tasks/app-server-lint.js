'use strict';

module.exports = function (gulp) {
    return function () {
        var plumber = require('gulp-plumber'),
            errorHandler = require('../gulp-error-handler'),
            eslint = require('gulp-eslint');

        return gulp.src([ 'server/**/*.js' ])
            .pipe(eslint())
            .pipe(eslint.format())
            .pipe(eslint.failAfterError());
    };
};
